document.addEventListener('jqueryVersionEvent', function (e)  {
    var data=e.detail;
    chrome.extension.sendMessage({
        action: "setSource",
        source: data
    });
});


function DOMtoString(document_root) {

    var script = document_root.getElementById('jQueryCustomEvent');
    if(script == null) {
        script = document.createElement("script");
        script.id = 'jQueryCustomEvent';
        script.innerHTML = "var evt=document.createEvent(\"CustomEvent\"); function GetJqueryVersion() { if(typeof(jQuery) != 'undefined') {  var data = jQuery.fn.jquery; evt.initCustomEvent(\"jqueryVersionEvent\", true, true, data); document.dispatchEvent(evt); } } javascript:GetJqueryVersion();";
        document_root.lastChild.appendChild(script);
    } else {
        location.href='javascript:GetJqueryVersion();javascript:void(0);'
    }
}

chrome.extension.sendMessage({
    action: "getSource",
    source: DOMtoString(document)
});