chrome.extension.onMessage.addListener(function(request, sender) {
    if (request.action == "setSource") {
        document.getElementById('jqueryVersion').innerHTML = request.source;
    }
});

function onWindowLoad() {
    document.getElementById('loader').style.backgroundImage = 'url('+chrome.extension.getURL('img/dancer.gif')+')';

    var httpReq = new window.XMLHttpRequest();
    httpReq.onreadystatechange = function(data) {
        var loader = document.getElementById('loader');
        loader.innerHTML = '';
        if (httpReq.readyState == 4) {
            var response = JSON.parse(httpReq.response);
            if(response.containers != null && response.containers[0].data != null && response.containers[0].data.rows != null && response.containers[0].data.rows[0].images != null) {
                var image = document.createElement('img');
                image.setAttribute('src', response.containers[0].data.rows[0].images.large);
                image.setAttribute('alt', '');

                image.onload = function() {
                    if(response.containers[0].data.rows[0].title != null && response.containers[0].data.title.length > 0) {
                        var title = document.createElement('h3');
                        title.innerHTML = response.containers[0].data.rows[0].title;
                        document.getElementById('loader').appendChild(title);
                    }
                };

                var a = document.createElement('a');
                a.appendChild(image);
                a.href = 'http://rodekassen.revert.dk/' + response.containers[0].data.rows[0].mediaId;
                a.target = '_blank';

                loader.appendChild(a);
            }
        }
    }
    httpReq.open("GET", "http://rodekassen.revert.dk/json/media.json?type=image&tag=babe");
    httpReq.send(null);

    chrome.tabs.executeScript(null, {
        file: "main.js"
    }, function() {
        // If you try and inject into an extensions page or the webstore/NTP you'll get an error
        if (chrome.extension.lastError) {
            message.innerText = 'There was an error injecting script : \n' + chrome.extension.lastError.message;
        }
    });
}

window.onload = onWindowLoad;